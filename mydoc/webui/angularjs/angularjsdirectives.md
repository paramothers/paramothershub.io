
## Predefined Directives

 **Directive can be either as element/attribute/class/comment**

 *** Directives are case insenstive***

 *** it extends HTML behaviour***


ngApp

ng-controller
ng-include -- it include html fragment

ng-hide
ng-show

ng-style -- it is better than ng-class
ng-class

ng-repeat

ng-model -- take data from view to controller by $scope
ng-bind  -- take data from controller to view by $scope
ng-bind-html

ng-if

ng-disabled
ng-options 

ng-click
