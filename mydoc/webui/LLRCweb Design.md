

### LLRCweb - EXT - JS usage




- [ ] No left side pane, since we display a lot of columns in both fileupload / OVT
- [ ] Create json file to demo or bug fix client side UI
- [ ] Server side completly separate by Ajax call..so, later Angular JS, can use if need
- [ ] identify to use mixin feature at middle of development
- [ ] Excel parsing at client side,Model validation in client side using Validation
- [ ] Dynamic class loading based on user role.
- [ ] User Never going to do both OVT and file upload at the same time. No MORE TABs
- [ ] Ignore excel's tab name validation, it going to do nothing with logic.
- [ ] separate CSS file for images class
- [ ] quick search in file type selection drop-down (use of local filtering)
- [ ] Use ListView for file upload
- [ ] lets use Ext.data.Types for declaring data type of fields in Model. it save us any mis spell
- [ ]  use of validation in model
- [ ] in OVT Pages, use feature of grid panel for summary group.
